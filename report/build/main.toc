\babel@toc {portuguese}{}
\contentsline {section}{\numberline {1}Introdução}{2}{section.1}%
\contentsline {paragraph}{}{2}{section*.3}%
\contentsline {section}{\numberline {2}Arquitetura conceptual}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}\textit {Client}}{2}{subsection.2.1}%
\contentsline {paragraph}{}{2}{section*.4}%
\contentsline {subsection}{\numberline {2.2}\textit {Server}}{3}{subsection.2.2}%
\contentsline {paragraph}{}{3}{section*.6}%
\contentsline {section}{\numberline {3}Protocolo de comunicação}{4}{section.3}%
\contentsline {paragraph}{}{4}{section*.8}%
\contentsline {section}{\numberline {4}Instalação}{4}{section.4}%
\contentsline {subsection}{\numberline {4.1}Dependências}{4}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Compilação}{5}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Execução}{5}{subsubsection.4.2.1}%
\contentsline {section}{\numberline {5}Exemplo Uso}{6}{section.5}%
\contentsline {subsection}{\numberline {5.1}Registo / \textit {Login}}{6}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Menu}{6}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Adicionar Contactos / Contactos}{7}{subsection.5.3}%
\contentsline {subsection}{\numberline {5.4}\textit {Set Infected} / Resultado Teste}{7}{subsection.5.4}%
\contentsline {subsection}{\numberline {5.5}\textit {Multicast} / \textit {Broadcast}}{8}{subsection.5.5}%
\contentsline {section}{\numberline {6}Conclusão}{8}{section.6}%
