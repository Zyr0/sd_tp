package client.threads;

import client.gui.Gui;

import java.net.Socket;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

/**
 * This Thread opens when user send user credentials,
 * using interface to show login or registration
 * result received from server and change view
 */
public class Session extends Thread {
    private final Socket socket;
    private final List<String> msg;
    private final Gui gui;
    private boolean logged;

    public Session(Socket socket, List<String> credentials, Gui gui) {
        super("Session Thread");
        this.logged = false;
        this.socket = socket;
        this.msg = credentials;
        this.gui = gui;
    }

    @Override
    public void run() {
        try {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            out.println(msg.get(0));
            out.println(msg.get(1));

            String response;
            while (!logged){
                response = in.readLine();
                if (response.equals("new")){
                    String county = gui.countyDialog();
                    if (county != null){
                        out.println(county);
                    }
                } else if (!response.equals("logged")){
                    gui.errorDialog("Wrong password");
                } else {
                    logged = true;
                }
            }

            String host = in.readLine();
            ThreadHelper.setMulticastHost(host);

            gui.setMenuLayout(("Hello,  " + in.readLine()));
            gui.msgDialog("Sucessfully Logged in");
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
