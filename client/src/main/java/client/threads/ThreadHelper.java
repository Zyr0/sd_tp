package client.threads;

import client.gui.Gui;
import java.net.Socket;
import java.net.InetAddress;
import java.io.IOException;
import java.util.List;

/**
 * This class open socket and manage all threads
 */
public class ThreadHelper {
    private static final int MULTICAST_PORT = 4446;
    private static final int BROADCAST_PORT = 4445;
    private static final String IP = "localhost";
    private static final int PORT = 7;

    private static Socket socket;
    private static String multicastHost;
    private static ReceiveBroadcast receiveBroadcast;
    private static ReceiveMulticast receiveMulticast;

    public static void startSocket() throws IOException {
        socket = new Socket(IP, PORT);
    }

    public static Socket getSocket() {
        return ThreadHelper.socket;
    }

    public static void startSession(Socket socket, List<String> msg, Gui gui) {
        Session session = new Session(socket, msg, gui);
        session.start();
    }

    public static void startMenu(Socket socket, String msg, Gui gui) {
        Menu menu = new Menu(socket, msg, gui);
        menu.start();
    }

    public static void setMulticastHost(String host) {
        ThreadHelper.multicastHost = host;
    }

    public static void startNotificationThreads(Socket socket, Gui gui) {
        try {
            ReadMessages readMessages = new ReadMessages(socket, gui);
            receiveBroadcast = new ReceiveBroadcast(IP, BROADCAST_PORT, gui);
            receiveMulticast = new ReceiveMulticast(MULTICAST_PORT, InetAddress.getByName(multicastHost), gui);
            readMessages.start();
            receiveBroadcast.start();
            receiveMulticast.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void stopNotificationThreads(boolean stop) {
        if (stop) {
            receiveBroadcast.setRunning(false);
            receiveMulticast.setRunning(false);
            receiveBroadcast.interrupt();
            receiveMulticast.interrupt();
        }
    }
}
