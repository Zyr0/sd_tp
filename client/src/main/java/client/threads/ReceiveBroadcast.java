package client.threads;

import java.io.IOException;
import java.net.InetAddress;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import client.gui.Gui;

/**
 * This Thread opens entering on menu layout
 * send empty packet via Broadcast to inform
 * server this client want to receive broadcast packets
 * Using interface to show info received
 */
public class ReceiveBroadcast extends Thread {
    private final String ip;
    private final int port;
    private boolean running;
    private final Gui gui;

    public ReceiveBroadcast(String ip, int port, Gui gui) {
        super("ReceiveBroadcast Thread");
        this.ip = ip;
        this.port = port;
        this.gui = gui;
    }

    public void setRunning(boolean running) { this.running = running; }

    @Override
    public void run() {
        try {
            running = true;
            byte[] buffer = new byte[1024];
            DatagramSocket socket = new DatagramSocket();

            while (running) {
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(ip), port);
                socket.send(packet);

                packet = new DatagramPacket(buffer, buffer.length);
                socket.receive(packet);
                String message = new String(buffer, 0, packet.getLength());
                gui.msgDialog(message);
            }
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Error reading from broadcast");
        }
    }
}
