package client.threads;

import client.gui.Gui;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * This Thread opens entering on menu layout
 * all messages received from server are
 * managed here using interface to show them
 */
public class ReadMessages extends Thread {
    private final Socket socket;
    private final Gui gui;

    public ReadMessages(Socket socket, Gui gui) {
        super("Read Messages Thread");
        this.socket = socket;
        this.gui=gui;
    }

    @Override
    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String res;
            while ((res = in.readLine()) != null && !res.equals("Bye!")){
                if(res.startsWith("|"))
                    gui.setContactLayout(res);
                else{
                    gui.msgDialog(res);
                }
            }

        } catch (IOException e) {
            System.err.println("Error getting to server.");
        }
    }
}
