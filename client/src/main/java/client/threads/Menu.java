package client.threads;

import client.gui.Gui;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * This Thread is opened when user is successfully registered or logged in
 * Use interface to change view and send command for server
 */
public class Menu extends Thread {
    private final Socket socket;
    private final String option;
    private final Gui gui;

    public Menu(Socket socket, String op, Gui gui) {
        super("Menu Thread");
        this.socket = socket;
        this.option= op;
        this.gui = gui;
    }

    @Override
    public void run() {
        try {
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println(option);
            if(option.equals("8")){
                ThreadHelper.stopNotificationThreads(true);
                gui.setLoginLayout();
                gui.setBarMsg(null);
            } else if (option.equals("3")){
                String contact = gui.userDialog();
                if (contact != null){
                    System.out.println(contact);
                    out.println(contact);
                } else {
                    out.println("Cancel");
                }
            }
        } catch (IOException e) {
            System.err.println("Error sending messages to the server.");
        }
    }
}
