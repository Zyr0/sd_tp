package client.threads;

import java.io.IOException;
import java.net.InetAddress;
import java.net.DatagramPacket;
import java.net.MulticastSocket;

import client.gui.Gui;

/**
 * This Thread opens entering on menu layout
 * waiting message from server via multicast socket
 * Using interface to show info received
 */
public class ReceiveMulticast extends Thread {
    private boolean running;
    private final int port;
    private final InetAddress address;
    private final Gui gui;

    public ReceiveMulticast(int port, InetAddress address, Gui gui) {
        super("Receive Multicast Thread");
        this.port = port;
        this.address = address;
        this.running = true;
        this.gui = gui;
    }

    public void setRunning(boolean running) { this.running = running; }

    @Override
    public void run() {
        try {
            MulticastSocket socket = new MulticastSocket(port);
            socket.joinGroup(address);

            byte[] buffer = new byte[1024];
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
            while (running) {
                socket.receive(packet);
                String message = new String(buffer, 0, packet.getLength());
                gui.msgDialog(message);
            }
            socket.close();
        } catch (IOException e) {
            System.err.println("Error getting to server.");
        }
    }
}
