package client.gui;

import javax.swing.*;
import java.util.*;

/**
 * This class is used on interface
 * to make menu layout components,
 */
public class MenuLayout extends JPanel{
    private final List<JButton> buttons= new ArrayList<>();

    public MenuLayout() {
        super();
        //labels
        setLayout(null);
        //Menu buttons
        buttons.add(new JButton("Set as Infected"));
        buttons.add(new JButton("Do Test"));
        buttons.add(new JButton("Add Contacts"));
        buttons.add(new JButton("List Contacts"));
        buttons.add(new JButton("Test result"));
        buttons.add(new JButton("How many infected (County)"));
        buttons.add(new JButton("How many infected (Region)"));
        buttons.add(new JButton("Log Out"));
        int yvar= 40;
        for(JButton b : buttons){
            b.setBounds(80,yvar, 185,20);
            yvar +=22;
            add(b);
        }
    }

    public List<JButton> getButtons() { return buttons; }
}
