package client.gui;

import client.threads.ThreadHelper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class contain all views used on JFrame (swing)
 * and is used to manage interface interactions
 * The listeners are controlled here
 * Using ThreadHelper to manage threads
 */
public class Gui {
    private final JFrame frame;
    private LoginLayout llayout;
    private MenuLayout mlayout;
    private ContactLayout clayout;
    private JLabel barText;
    private String text;
    private boolean onregist;

    public Gui() {
        frame = new JFrame("Away from me Covid");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        setBar();
        setLoginLayout();
        frame.setVisible(true);
    }

    public void setLoginLayout(){
        resetView();
        frame.setSize(350, 300);
        llayout = new LoginLayout();
        llayout.getButton().addActionListener( this::credentialsListener );
        frame.add(llayout);
        setBarMsg(null);
    }

    public void setMenuLayout(String mensage){
        text=mensage;
        resetView();
        frame.setSize(350, 350);
        mlayout = new MenuLayout();
        int op = 1;
        for (JButton b : mlayout.getButtons()){
                b.setActionCommand(String.valueOf(op));
                b.addActionListener(this::menuListener);
                op++;
        }
        if (llayout != null) frame.remove(llayout);
        frame.add(mlayout);
        setBarMsg(mensage);
        ThreadHelper.startNotificationThreads(ThreadHelper.getSocket(), this);
    }

    public void credentialsListener (ActionEvent e){
        List<String> msg = new ArrayList<>();
        msg.add(llayout.getUserText().getText());
        msg.add(llayout.getPassText().getText());
        try {
            ThreadHelper.startSocket();
            ThreadHelper.startSession(ThreadHelper.getSocket(), msg, this);
        } catch(IOException o) {
            System.err.println("Couldn't get I/O for the connectin.");
            System.exit(1);
        }
    }

    public void menuListener (ActionEvent e){
        JButton button = (JButton) e.getSource();
        String command = button.getActionCommand();
        ThreadHelper.startMenu(ThreadHelper.getSocket(), command, this);
    }

    public void setBarMsg(String msg) {
        if(msg == null){
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            msg = df.format(new Date());
            barText.setText(msg);
        }else {
            barText.setText(msg);
        }
    }

    public void msgDialog(String msg){
        JOptionPane.showMessageDialog(this.frame,msg);
    }

    public void errorDialog(String message){
        JOptionPane optionPane = new JOptionPane(message, JOptionPane.ERROR_MESSAGE);
        JDialog dialog = optionPane.createDialog("Fail");
        dialog.setAlwaysOnTop(true);
        dialog.setVisible(true);
    }

    public String countyDialog(){
        String county;
        if(!onregist){
            county = JOptionPane.showInputDialog(frame, "Regist typing your county");
            onregist = true;
        } else
            county = JOptionPane.showInputDialog(frame, "Type a valid county!");
        if(county == null)
            onregist = false;
        return county;
    }

    public String userDialog(){
        String name = JOptionPane.showInputDialog(frame, "Type user name: ");
        System.out.println("DIALOG: "+name);
        return name;
    }

    public void setContactLayout(String contacts) {
        resetView();
        clayout = new ContactLayout(contacts);
        clayout.getBbutton().addActionListener(l->{
            setMenuLayout(text);
        });
        frame.add(clayout);
        setBarMsg("My Contacts");
    }

    public void resetView(){
        if (llayout != null) frame.remove(llayout);
        if (mlayout != null) frame.remove(mlayout);
        if (clayout != null) frame.remove(clayout);
    }

    public void setBar(){
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu();
        menuBar.add(fileMenu);
        frame.setJMenuBar(menuBar);
        JToolBar toolbar = new JToolBar();
        barText = new JLabel();
        toolbar.add(barText);
        frame.add(toolbar, BorderLayout.NORTH);
    }
}
