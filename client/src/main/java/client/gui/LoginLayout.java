package client.gui;

import javax.swing.*;

/**
 * This class is used on interface
 * to make login layout components,
 */
public class LoginLayout extends JPanel {
    private JLabel nameLabel;
    private JLabel passLabel;
    private JTextField userText;
    private JPasswordField passText;
    private JButton button;

    public LoginLayout() {
        super();
        //labels
        setLayout(null);
        nameLabel = new JLabel("Name:");
        nameLabel.setBounds(20, 20, 80, 25);
        passLabel = new JLabel("Password:");
        passLabel.setBounds(20, 50, 80, 25);
        //fields
        userText = new JTextField(20);
        userText.setBounds(100, 20, 165, 25);
        passText = new JPasswordField(20);
        passText.setBounds(100, 50, 165, 25);
        //submit
        button = new JButton("Login");
        button.setBounds(100, 85, 80, 25);

        add(nameLabel);
        add(userText);
        add(passLabel);
        add(passText);
        add(button);
    }

    public JButton getButton() { return button; }
    public JTextField getUserText() { return userText; }
    public JPasswordField getPassText() { return passText; }


}
