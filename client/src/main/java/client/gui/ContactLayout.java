package client.gui;

import javax.swing.*;
import javax.swing.plaf.basic.BasicArrowButton;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is used on interface
 * to make contact layout components,
 */
public class ContactLayout extends JPanel {

    private final BasicArrowButton bbutton;

    public ContactLayout(String msg) {
        super();
        setSize(350, 350);
        setLayout(null);
        List<JLabel> contacts = new ArrayList<>();
        for (String sub : msg.split("\\|", -1)){
            System.out.println(sub);
            contacts.add(new JLabel(sub));
        }
        int yvar= 40;
        for(JLabel name : contacts){
            name.setBounds(60,yvar, 185,20);
            yvar +=22;
            add(name);
        }
        bbutton = new BasicArrowButton(BasicArrowButton.WEST);
        bbutton.setBounds(15, 15, 35, 20);
        add(bbutton);
    }
    public JButton getBbutton() { return bbutton; }

}
