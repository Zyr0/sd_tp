package server;

import server.user.User;
import server.regions.County;
import server.regions.Region;
import server.threads.NotifyContact;

import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import java.net.InetAddress;

public class Base {
    private HashMap<InetAddress, Integer> hosts;
    public List<Region> regions;
    public List<User> users;

    public Base(List<Region> regions, List<User> users) {
        this.regions = regions;
        if (users != null)
            this.users = users;
        else
            this.users = new ArrayList<>();
        this.hosts = new HashMap<>();
    }

    public List<User> getUsers() { return this.users; }
    public List<Region> getRegions() { return this.regions; }
    public HashMap<InetAddress, Integer> getHosts() { return this.hosts; }

    /**
     * add a host to hosts list
     * @param host - the host to broadcast to
     */
    public synchronized void addHost(InetAddress host, int port) {
        hosts.put(host, port);
    }


    /**
     * @param userId userId
     * @return user, if doesnt exist return null
     */
    public synchronized User getUser(int userId){
        for (User u: users){
            if (u.getId() == userId){
                return u;
            }
        }
        return null;
    }

    /**
     * get a user from the username
     * @param name - the name of the user
     * @return user, if doesnt exist return null
     */
    public synchronized User getUser(String name){
        for (User u: users){
            if (u.getName().equals(name)){
                return u;
            }
        }
        return null;
    }

    /**
     * Verify user on login
     * @param name name
     * @param password password
     * @return user, if doesnt exist return null
     */
    public synchronized User getUser(String name, String password){
        for (User u : users){
            if (u.getName().equals(name) && u.getPassword().equals(password)){
                return u;
            }
        }
        return null;
    }

    /**
     * Regist new user
     * @param name name
     * @param password password
     * @param county county
     * @return registed user
     */
    public synchronized User setUser(String name, String password, County county){
        User user = new User(name, password, county);
        users.add(user);
        return user;
    }

    /**
     * Return
     * @param countyId id
     * @return number of people infected from (param) county
     */
    public synchronized int countByCounty(int countyId){
        int n = (int) users.stream().filter(
                u -> u.getCounty().getId() == countyId)
                .filter(User::isInfected).count();
        return n;
    }

    /**
     * @param region name
     * @return number of people infected from (param) region
     */
    public synchronized int countByRegion(String region){
        int n = (int) users.stream().filter(
                u -> u.getCounty().getRegion().equals(region))
                .filter(User::isInfected).count();
        return n;
    }

    /**
     * Manda notifycação aos contactos do utilizador
     * @param user - o utilizador com os contactos
     */
    public synchronized void notifyContacts(User user) {
        user.getContacts().forEach(c -> {
            final String message = "O utilizador " + user.getName() + " esta infetado";
            new NotifyContact(c.getUser().getSocket(), message).start();
        });
    }
}
