package server.user;

import java.util.concurrent.atomic.AtomicInteger;

public class Contact {
    private static final AtomicInteger count = new AtomicInteger(0);
    private int id;
    private User user;

    public Contact(User user) {
        this.id = count.getAndIncrement();
        this.user = user;
    }

    public Contact(int id, User user) {
        this.user = user;
        this.id = id;
        if (count.get() <= id) {
            count.set(id);
        }
    }

    public int getId() { return this.id; }
    public User getUser() { return this.user; }
}
