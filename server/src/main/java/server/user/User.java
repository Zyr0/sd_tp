package server.user;

import server.regions.Region;
import server.regions.County;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.List;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class User {
    private static final AtomicInteger count = new AtomicInteger(0);
    private int id;
    private String name;
    private String password;
    private List<Contact> contacts;
    private List<Integer> contacts_ids;
    private int county_id;
    private County county;
    private boolean infected;
    private boolean waitRes;
    private boolean logged;
    private Socket socket;

    public User(String name, String password, County county) {
        this.id = count.getAndIncrement();
        this.logged = true;
        this.name = name;
        this.password = password;
        this.infected = false;
        this.waitRes = false;
        this.county = county;
        this.contacts = new ArrayList<>();
    }

    public User(JSONObject obj) {
        this.id = obj.getInt("id");
        this.name = obj.getString("name");
        this.password = obj.getString("password");
        this.infected = obj.getBoolean("infected");
        this.county_id = obj.getInt("county");
        JSONArray con = obj.getJSONArray("contacts");
        contacts_ids = new ArrayList<>();
        for (int i = 0; i < con.length(); i++) {
            contacts_ids.add(con.getInt(i));
        }
        this.logged = false;
        this.waitRes = false;
        count.set(this.id); /* set counter to proper count */
    }

    /**
     * When reading users set the contacts and the regions right
     * @param users - list of users
     * @param regions - list of regions
     */
    public void setRight(List<User> users, List<Region> regions) {
        contacts = new ArrayList<>();
        if (!contacts_ids.isEmpty()) {
            for (int i = 0; i < contacts_ids.size(); i++) {
                for (int j = 0; j < users.size(); j++) {
                    if (contacts_ids.get(i) == users.get(j).getId()) {
                        contacts.add(new Contact(contacts_ids.get(i), users.get(j)));
                    }

                }
            }
        }
        for (int i = 0; i < regions.size(); i++) {
            county = regions.get(i).getCounty(county_id);
            if (county != null)
                break;
        }
    }

    public int getId() { return this.id; }
    public String getName() { return this.name; }
    public String getPassword() { return this.password; }
    public List<Contact> getContacts() { return this.contacts; }
    public County getCounty() { return this.county; }
    public boolean isLogged() { return this.logged; }
    public boolean isInfected() { return this.infected; }
    public boolean isWaitingRes() { return this.waitRes; }
    public Socket getSocket() { return this.socket; }

    public synchronized void setLogged(boolean logged) { this.logged = logged; }
    public synchronized void setSocket(Socket socket) { this.socket = socket; }
    public synchronized void setWaitingRes(boolean waitRes) { this.waitRes = waitRes; }
    public synchronized void setInfected(boolean infected) { this.infected = infected; }
    public synchronized void addContact(User user) {
        contacts.add(new Contact(user));
    }

    public boolean hasContact(String name){
        for (Contact c : contacts)
            if (c.getUser().getName().equals(name))
                return true;
        return false;
    }
}
