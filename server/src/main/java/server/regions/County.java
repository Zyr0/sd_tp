package server.regions;

public class County {
	private int id;
	private String name;
	private String region;

	public County(int id, String name, String region) {
		this.id = id;
		this.name = name;
		this.region = region;
	}

    public String getHost() {
        return  "230.0.0." + this.id;
    }

	public int getId() { return this.id; }
	public void setId(int id) { this.id = id; }
	public String getName() { return this.name; }
	public void setName(String name) { this.name = name; }
	public String getRegion() { return region; }
}
