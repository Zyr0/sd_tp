package server.regions;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;


public class Region {
	private String name;
	private List<County> counties;

	/**
	 * Default Constructor
	 * @param name the name of the region
	 * @param counties the list of counties of the region
	 */
	public Region(String name, List<County> counties) {
		this.name = name;
		this.counties = counties;
	}

	/**
	 * Constructor
	 * @param name the name of the region
	 * @param arr a json array to convert to counties
	 */
	public Region(String name, JSONArray arr) {
		this.name = name;
		this.counties = new ArrayList<>();
		int size = arr.length();
		for (int i = 0; i < size; i++) {
			JSONObject obj = arr.getJSONObject(i);
			Set<String> obj_keys = obj.keySet();
			obj_keys.forEach((c) -> {
				this.counties.add(new County(Integer.parseInt(c), obj.getString(c), name));
			});
		}
	}

	public String getName() { return this.name; }
	public List<County> getCounties() { return this.counties; }

	/**
	 * Return county if exists, null if not
	 * @param name county name
	 * @return c
	 */
	public County getCounty(String name){
		for (County c : counties){
			if (c.getName().equals(name)){
				return c;
			}
		}
		return null;
	}

	/**
	 * Return county if exists, null if not
	 * @param id countyId
	 * @return c
	 */
	public County getCounty(int id){
		for (County c : counties){
			if (c.getId()== id){
				return c;
			}
		}
		return null;
	}
}
