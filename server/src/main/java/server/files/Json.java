package server.files;

import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import server.regions.Region;
import server.user.User;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONTokener;

public class Json {

    /**
     * read the users from filename
     * @param filename - the file to read the users from
     * @param regions - a list of regions
     * @return the list of users or null if error reading the file
     */
    public static List<User> readUsers(String filename, List<Region> regions) {
        try {
            List<User> users = new ArrayList<>();
            JSONObject obj = readFile(filename, false);
            JSONArray arr = obj.getJSONArray("users");
            int size = arr.length();
            for (int i = 0; i < size; i++)
                users.add(new User(arr.getJSONObject(i)));
            users.forEach(u -> {
                u.setRight(users, regions);
            });
            return users;
        } catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

	/**
	 * read the regions from resources folder
     * @param filename - the filename to read regions from
	 * @return the List of regions or null if error reading the file
	 */
	public static List<Region> readRegions(String filename) {
		try {
			List<Region> regions = new ArrayList<>();
			JSONObject obj = readFile(filename, true);
			Set<String> arr_regions = obj.keySet();
			arr_regions.forEach((k) -> {
				regions.add(new Region(k, obj.getJSONArray(k)));
			});
			return regions;
		} catch (IllegalArgumentException e) {
			System.err.println(e.getMessage());
		}
		return null;
	}

	/**
	 * read a file from the resources folder
	 * @param filename the file name to read
     * @param resources if the file is in the resources folder
	 * @return the first JSONObject of the file
	 * @throws IllegalArgumentException if file not found
	 */
	public static JSONObject readFile(String filename, boolean resources) throws IllegalArgumentException {
        try {
            InputStream in;
            if (resources)
                in = Json.class.getClassLoader().getResourceAsStream(filename);
            else
		        in = new FileInputStream(filename);
            JSONTokener tokener = new JSONTokener(in);
            return new JSONObject(tokener);
        } catch (FileNotFoundException e) {
			throw new IllegalArgumentException("file not found! " + filename);
        }
	}

    /**
     * Save users to json file
     * @param users - the users to save to file
     * @return true if success
     */
    public static boolean saveUsers(List<User> users, String filename) {
        try {
            JSONObject jsonUsers = new JSONObject();
            JSONArray arrUsers = new JSONArray();

            users.forEach(u -> {
                JSONObject user = new JSONObject();
                user.put("id", u.getId());
                user.put("name", u.getName());
                user.put("password", u.getPassword());
                user.put("county", u.getCounty().getId());
                user.put("infected", u.isInfected());
                JSONArray contacts = new JSONArray();
                u.getContacts().forEach(c -> {
                    contacts.put(c.getId());
                });
                user.put("contacts", contacts);
                arrUsers.put(user);
            });

            jsonUsers.put("users", arrUsers);
            FileWriter f = new FileWriter(filename);
            f.write(jsonUsers.toString(2));
            f.flush();
            f.close();
            return true;
        } catch(IOException e) {
            return false;
        }
    }
}
