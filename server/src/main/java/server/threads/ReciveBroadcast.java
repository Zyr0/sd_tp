package server.threads;

import server.Base;

import java.net.InetAddress;
import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.SocketException;
import java.io.IOException;

/**
 * This thread receive empty packet
 * for save client addresses on base hosts
 * use sleep timer defined(app)
 */
public class ReciveBroadcast extends Thread {
    private boolean running;
    private int port, sleep;
    private Base base;

    public ReciveBroadcast(int port, int sleep, Base base) {
        super("ReciveBroadcast Thread");
        this.port = port;
        this.sleep = sleep;
        this.base = base;
        this.running = true;
    }

    public void setRunning(boolean running) { this.running = running; }

    @Override
    public void run() {
        try {
            DatagramSocket socket = new DatagramSocket(port);
            try {
                byte[] buffer = new byte[1024];
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

                while(running) {
                    socket.receive(packet);
                    InetAddress address = packet.getAddress();
                    base.addHost(address, packet.getPort());
                    Thread.sleep(sleep);
                }

            } catch (IOException e) {
                System.out.println("\nCould not recive broadcast message");
            } catch (InterruptedException e) {
                System.err.println("\nError interrupting " + Thread.currentThread().getName());
            } finally {
                socket.close();
            }
        } catch(SocketException e) {
            System.err.println("\nSocket error in " + Thread.currentThread().getName());
        }
    }
}
