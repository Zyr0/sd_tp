package server.threads;

import server.Base;
import server.regions.County;

import java.net.InetAddress;
import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.SocketException;
import java.io.IOException;

/**
 * This thread send number of infected users
 * for each county multicast address
 * use sleep timer defined(app)
 */
public class SendMulticast extends Thread {
    private boolean running;
    private final int port;
    private final int sleep;
    private final Base base;
    private final County county;

    public SendMulticast(int port, int sleep, Base base, County county) {
        super("Send Multicast Thread for " + county.getName());
        this.port = port;
        this.sleep = sleep;
        this.base = base;
        this.county = county;
        this.running = true;
    }

    public void setRunning(boolean running) { this.running = running; }

    @Override
    public void run() {
        try {
            DatagramSocket socket = new DatagramSocket();
            try {
                while(running) {
                    int n = base.countByCounty(county.getId());
                    final byte[] s = ("There are " + n + " cases in " + county.getName()).getBytes();
                    InetAddress address = InetAddress.getByName(county.getHost());
                    DatagramPacket packet = new DatagramPacket(s, s.length, address, port);
                    socket.send(packet);
                    Thread.sleep(sleep);
                }
            } catch (IOException e) {
                System.err.println("\nCould not multicast message");
            } catch (InterruptedException e) {
                System.err.println("\nError interrupting " + Thread.currentThread().getName());
            } finally {
                socket.close();
            }
        } catch(SocketException e) {
            System.err.println("\nSocket error in " + Thread.currentThread().getName());
        }
    }
}
