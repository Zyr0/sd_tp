package server.threads;

import java.net.Socket;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * This thread Notify user contacts about disease
 */
public class NotifyContact extends Thread{
    private final Socket socket;
    private final String message;

	public NotifyContact(Socket socket, String message) {
		super("NotifyContact");
        this.socket = socket;
        this.message = message;
	}

    @Override
	public void run() {
        try {
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println(message);
        } catch (IOException e) {
            System.err.println("Error sending information to user");
        }
	}
}
