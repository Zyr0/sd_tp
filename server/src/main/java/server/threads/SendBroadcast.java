package server.threads;

import server.Base;

import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.SocketException;
import java.io.IOException;

/**
 * This thread send for all hosts
 * number of infected users on their region
 * use sleep timer defined(app)
 */
public class SendBroadcast extends Thread {
    private boolean running;
    private final int sleep;
    private final Base base;

    public SendBroadcast(int sleep, Base base) {
        super("Send Broadcast Thread");
        this.sleep = sleep;
        this.base = base;
        this.running = true;
    }

    public void setRunning(boolean running) { this.running = running; }

    @Override
    public void run() {
        try {
            DatagramSocket socket = new DatagramSocket();
            try {
                socket.setBroadcast(true);

                while(running) {
                    base.getRegions().forEach(r -> {
                        int n = base.countByRegion(r.getName());
                        final byte[] s = ("There are " + n + " cases in " + r.getName()).getBytes();
                        base.getHosts().forEach((h, p) -> {
                            DatagramPacket packet = new DatagramPacket(s, s.length, h, p);
                            try {
                                socket.send(packet);
                            } catch (IOException i) { i.printStackTrace(); }
                        });
                        base.getHosts().clear();
                    });
                    Thread.sleep(sleep);
                }
            } catch (IOException e) {
                System.out.println("\nCould not send broadcast message");
            } catch (InterruptedException e) {
                System.err.println("\nError interrupting " + Thread.currentThread().getName());
            } finally {
                socket.close();
            }
        } catch(SocketException e) {
            System.err.println("\nSocket error in " + Thread.currentThread().getName());
        }
    }
}
