package server.threads;

import server.Base;
import server.user.User;
import server.regions.Region;
import server.regions.County;

import java.io.*;
import java.net.Socket;

/**
 * This thread manage user interaction
 * send and receives messages for
 * registration or login, and menu options
 */
public class UserThread extends Thread {
    private final Socket socket;
    private final Base base;

    public UserThread(Socket socket, Base base) {
        this.socket = socket;
        this.base = base;
    }

    @Override
    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            String name = in.readLine();
            String password = in.readLine();

            User user = base.getUser(name);
            if (user == null) {
                County c = null;
                do {
                    out.println("new");
                    String input = in.readLine();
                    for (Region reg: base.regions) {
                        if (reg.getCounty(input) != null){
                            c = reg.getCounty(input);
                        }
                    }
                } while (c == null);
                user = base.setUser(name, password, c);
            } else if (!user.getPassword().equals(password)) {
                do {
                    out.println("reType");
                    password = in.readLine();
                } while ((user = base.getUser(name, password)) == null);
            }

            user.setSocket(socket);
            out.println("logged");
            out.println(user.getCounty().getHost());
            out.println(user.getName());

            String message = null;
            while((message = menu(in.readLine(), user)) != null)
                if (!message.equals("Cancel"))
                    out.println(message);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String menu(String response, User user) throws IOException {
        String message="Cancel";
        if (response == null) {
            return message;
        }
        switch (Integer.parseInt(response)){
            case 1:
                user.setInfected(true);
                message = "Your infected state has been saved";
                base.notifyContacts(user);
                break;
            case 2:
                if (user.isWaitingRes()){
                    message = "Wait for result to come!.";
                } else {
                    new TestThread(base, user).start();
                    message = "Success, wait for your result";
                }
                break;
            case 3:
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()));
                String name = in.readLine();
                if(!name.equals("Cancel")){
                    User u = base.getUser(name);
                    if (u == null){
                        message = "User doesn't exist";
                    }else{
                        if (!user.getName().equals(name)) {
                            if (!user.hasContact(name)){
                                user.addContact(u);
                                message = "Success! "+u.getName() + " added !";
                            }else {
                                message = "Already on your contact list";
                            }
                        } else {
                            message = "You can't add yourself";
                        }
                    }
                }
                break;
            case 4:
                if (user.getContacts().size() > 0) {
                    StringBuilder builder = new StringBuilder();
                    user.getContacts().forEach(c -> {
                        final User usr = c.getUser();
                        builder.append("|").append(usr.getName());
                    });
                    message = builder.toString();
                    System.out.println("MENSAGE"+message);
                } else {
                    message = "You have no contacts";
                }
                break;
            case 5:
                if (user.isWaitingRes()){
                    message = "Wait for result to come!.";
                }else{
                    if (user.isInfected())
                        message = "You have covid.";
                    else
                        message = "You dont have covid, stay safe!";
                }
                break;
            case 6:
                County c = user.getCounty();
                message = "In " + c.getName() + " there are " + base.countByCounty(c.getId()) + " people infected!";
                break;
            case 7:
                String region = user.getCounty().getRegion();
                message = "In " + region + " there are " + base.countByRegion(region) + " people infected!";
                break;
            case 8:
                message = "Bye!";
                break;
            default:
                break;
        }
        return message;
    }
}
