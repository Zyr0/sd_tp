package server.threads;

import server.Base;

import java.io.*;
import java.net.Socket;
import java.net.ServerSocket;

/**
 * This thread is waiting for clients to connect with server
 * Accept connection and launch userThread for each session
 */
public class Session extends Thread{
    private boolean running;
    private int port;
    private ServerSocket server;
    public Base base;

    public Session(int port, Base base) {
        super("Session");
        this.port = port;
        this.base = base;
        this.running = true;
    }

    public void setRunning(boolean running) { this.running = running; }

    public void run() {
        try {
            server = new ServerSocket(port);
            System.out.println(">> Waiting for user\n");
            while (running){
                Socket socket = server.accept();
                UserThread userThread = new UserThread(socket, base);
                userThread.start();
            }
        } catch (IOException e) {
            System.err.println("\nCould not listen on port: " + port);
            System.exit(-1);
        } finally {
            try {
                server.close();
            } catch(IOException e) {
                System.err.println("\nCould not close socket in " + Thread.currentThread().getName());
            }
        }
    }
}
