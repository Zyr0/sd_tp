package server.threads;

import server.Base;
import server.user.User;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

/**
 * This thread is used for set random test result
 * and send user result notification
 */
public class TestThread extends Thread {
    private static final int SLEEP = 10000;
    private User user;
    private Base base;

    public TestThread(Base base, User user) {
        this.base = base;
        this.user = user;
    }

    @Override
    public void run() {
        try {
            user.setWaitingRes(true);
            Thread.sleep(SLEEP);

            Random random = new Random();
            user.setInfected(random.nextBoolean());
            user.setWaitingRes(false);

            String message = "not infected";
            if (user.isInfected()) {
                base.notifyContacts(user);
                message = "infected";
            }

            PrintWriter out = new PrintWriter(user.getSocket().getOutputStream(), true);
            out.println("You are " + message);

        } catch (InterruptedException | IOException e) {
            user.setWaitingRes(false);
            e.printStackTrace();
        }
    }
}
